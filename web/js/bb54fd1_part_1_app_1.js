var App = Backbone.View.extend({
	
	tagName: "tr",
	
	$rates: $('#rates'),
	template: _.template($('#rates-template').html()),
	
	events: {
		'click .sidebar-item':      'createTableView',
		       
	},
		
	initialize: function() {	
       
		this.types = {};
		
		this.config = {
			initial_url: ''
		};	
        console.log('init');
        
	},
	
	init: function() {
        
		// models
		this.rates = new this.types.Rates();
		
		// view
		this.listenTo(this.tables, 'sync', this.render);
		this.listenTo(this.tables, 'add', this.render);
		this.listenTo(this.tables, 'remove', this.render);
	},

    render: function() {

    },
	
	prepareUrl: function(url) {
		return this.config.initial_url + (url.startsWith('/') ? url : '/' + url);
	},	
});

var app = new App();

