<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Date;
use AppBundle\Entity\exchange;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    public function fetchExchangeListAction(Request $request)
    {
        $content = $request->getContent();
        $json = json_decode($content);
        $date = $json->date;
        $force = $json->force;

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://hnbex.eu/api/v1/rates/daily/?date=$date",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $exchange_array  = json_decode($response, true);;
        
        if($force == true) {
            $listByDate = $this->getDoctrine()
                ->getRepository('AppBundle:exchange')
                ->findByFetchDate(new \DateTime($date));

            if($listByDate) {
                $em = $this->getDoctrine()->getManager();
                
                foreach($listByDate as $row) {
                    $em->remove($row);
                    $em->flush();
                }
            }
        }
        foreach($exchange_array as $currency) {

            $exchange = new exchange();
            $exchange->setUnitValue($currency['unit_value']);
            $exchange->setBuyingRate($currency['buying_rate']);
            $exchange->setMedianRate($currency['median_rate']);
            $exchange->setSellingRate($currency['selling_rate']);
            $exchange->setCurrencyCode($currency['currency_code']);
            $exchange->setFetchDate(new \DateTime($date));
            $em = $this->getDoctrine()->getManager();
            $em->merge($exchange);
            $em->flush();
        }

        return new JsonResponse($exchange_array);
    }

    public function fetchHistoryAction(Request $request)
    {
        $content = $request->getContent();
        $json = json_decode($content);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT e.currencyCode AS currency, e.unitValue AS unitValue, e.buyingRate AS buyingRate, e.medianRate AS medianRate, e.sellingRate AS sellingRate, e.fetchDate AS onDate
               FROM AppBundle:exchange e
           ORDER BY e.fetchDate DESC'
        );

        $rates = $query->getScalarResult();
       
        return new JsonResponse($rates);
    }

    public function fetchExchangeFromDatabaseAction(Request $request)
    {
        $content = $request->getContent();
        $json = json_decode($content);

        $dateFrom = new \DateTime($json->dateFrom);
        $dateTo = new \DateTime($json->dateTo);
        $currencyCode = $json->currency;
        $parameters = array(
            'currencyCode' => $currencyCode, 
            'fetchDateFrom' => $dateFrom, 
            'fetchDateTo' => $dateTo
        );

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT e.currencyCode AS currency, e.unitValue AS unitValue, e.buyingRate AS buyingRate, e.medianRate AS medianRate, e.sellingRate AS sellingRate, e.fetchDate AS onDate
               FROM AppBundle:exchange e
              WHERE e.currencyCode = :currencyCode
                AND e.fetchDate >= :fetchDateFrom
                AND e.fetchDate <= :fetchDateTo
           ORDER BY e.fetchDate DESC'
        )->setParameters($parameters);

        $rates = $query->getScalarResult();
       
        return new JsonResponse($rates);
    }
}
