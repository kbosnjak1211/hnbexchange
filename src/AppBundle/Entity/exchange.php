<?php

namespace AppBundle\Entity;

/**
 * exchange
 */
class exchange
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $currencyCode;

    /**
     * @var int
     */
    private $unitValue;

    /**
     * @var string
     */
    private $buyingRate;

    /**
     * @var string
     */
    private $medianRate;

    /**
     * @var string
     */
    private $sellingRate;

    /**
     * @var \DateTime
     */
    private $fetchDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return exchange
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set unitValue
     *
     * @param integer $unitValue
     *
     * @return exchange
     */
    public function setUnitValue($unitValue)
    {
        $this->unitValue = $unitValue;

        return $this;
    }

    /**
     * Get unitValue
     *
     * @return int
     */
    public function getUnitValue()
    {
        return $this->unitValue;
    }

    /**
     * Set buyingRate
     *
     * @param string $buyingRate
     *
     * @return exchange
     */
    public function setBuyingRate($buyingRate)
    {
        $this->buyingRate = $buyingRate;

        return $this;
    }

    /**
     * Get buyingRate
     *
     * @return string
     */
    public function getBuyingRate()
    {
        return $this->buyingRate;
    }

    /**
     * Set medianRate
     *
     * @param string $medianRate
     *
     * @return exchange
     */
    public function setMedianRate($medianRate)
    {
        $this->medianRate = $medianRate;

        return $this;
    }

    /**
     * Get medianRate
     *
     * @return string
     */
    public function getMedianRate()
    {
        return $this->medianRate;
    }

    /**
     * Set sellingRate
     *
     * @param string $sellingRate
     *
     * @return exchange
     */
    public function setSellingRate($sellingRate)
    {
        $this->sellingRate = $sellingRate;

        return $this;
    }

    /**
     * Get sellingRate
     *
     * @return string
     */
    public function getSellingRate()
    {
        return $this->sellingRate;
    }

    /**
     * Set fetchDate
     *
     * @param \DateTime $fetchDate
     *
     * @return exchange
     */
    public function setFetchDate($fetchDate)
    {
        $this->fetchDate = $fetchDate;

        return $this;
    }

    /**
     * Get fetchDate
     *
     * @return \DateTime
     */
    public function getFetchDate()
    {
        return $this->fetchDate;
    }
}

