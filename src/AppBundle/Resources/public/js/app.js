var App = Backbone.View.extend({
	
	$rates: $('#rates'),
	template: _.template($('#rates-template').html()),
    $navigation: $('#navigation'),
	navigationTemplate: _.template($('#navigation-template').html()),
    ratesHistoryTemplate: _.template($('#rates-history-template').html()),
	
	events: {
        'click #getCurrency':           'getCurency',
        'click #getCurrencyFromApi':    'getCurrencyFromApi',
        'click #convertToKN':           'convertToKN'
	},
		
	initialize: function() {	
       
		this.types = {};
		this.config = {
			initial_url: ''
		};
        this.selected = 'EUR';
        this.selectedCurrencyToConvert = 'EUR'
	},
	
	init: function() {
        
        this.navigation();
		// models
		this.rates = new this.types.Rates();
		
		// view
		this.listenTo(this.rates, 'sync', this.render);
		this.listenTo(this.rates, 'add', this.render);
		this.listenTo(this.rates, 'remove', this.render);
	},

    render: function() {
        this.$rates.html(this.ratesHistoryTemplate({models: this.rates.models}));
    },

    openDatePicker: function() {
        var self = this;
        $( ".pickDateFrom" ).datepicker({
            inline: true
        });
        $( ".pickDateTo" ).datepicker({
            inline: true
        });
        $( ".pickDate" ).datepicker({
            inline: true
        });
        
        this.$('.curencyValues li a').unbind('click').click(function(e){
            e.preventDefault();
            self.selected = $(this).text();
            $(this).parents('.currencyPick').find('.dropdown-toggle').html(self.selected+' <span class="caret"></span>');
        
        }); 
        this.$('.curencyValuesConvert li a').unbind('click').click(function(e){
            e.preventDefault();
            self.selectedCurrencyToConvert = $(this).text();
            $(this).parents('.currencyForConvert').find('.dropdown-toggle').html(self.selectedCurrencyToConvert+' <span class="caret"></span>');
        
        }); 
    },
	
	prepareUrl: function(url) {
		return this.config.initial_url + (url.startsWith('/') ? url : '/' + url);
	},

    navigation: function() {
        this.$el.html(this.navigationTemplate());
        this.$navigation.html(this.$el);  
        this.openDatePicker();   
    },

    getCurency: function() {
        var self = this;
        var pickDateFrom = this.$('.pickDateFrom').val();
        var pickDateTo = this.$('.pickDateTo').val();
        var currency = this.selected;
        console.log(pickDateFrom,pickDateTo,currency);

        $.ajax({
            context: this,
            url: app.prepareUrl('/rates'),                             
            method: 'POST',
            data: JSON.stringify({dateFrom: pickDateFrom, dateTo: pickDateTo, currency: currency}),
            contentType: 'application/json',
            dataType: 'json',
            success: function(response, status, jqxhr) {
                self.renderFromResponse(response);
                
            },
            error: function(jqXhr, status, description) {
                console.log('ERRRR', jqXhr.responseJSON.error);
            }
        });
    },

    getCurrencyFromApi: function() {
        var self = this;
        var pickDate = this.$('.pickDate').val();
        var force = false;
        if(this.$( "#force" ).prop('checked') === true) {
            force = true;
        }
        var currency = this.selected;

        $.ajax({
            context: this,
            url: app.prepareUrl('/fetchExchange'),                             
            method: 'POST',
            data: JSON.stringify({date: pickDate, force: force}),
            contentType: 'application/json',
            dataType: 'json',
            success: function(response, status, jqxhr) {
                self.renderFromResponse(response);
            },
            error: function(jqXhr, status, description) {
                console.log('ERRRR', jqXhr.responseJSON.error);
            }
        });
    },

    convertToKN: function() {
        var median = this.rates.where({ currency: this.selectedCurrencyToConvert });
        // last median rate
        var medianForConvert = median[0].attributes.medianRate;
        var insertedValue = this.$('.insertedValue').val();
        var converted = parseFloat(insertedValue) * parseFloat(medianForConvert);
        this.$('.convertedValue').val(converted);
    },

    renderFromResponse: function(models) {
        this.$rates.html(this.template({models: models}));
    },

});

var app = new App();

$(document).ready(function() {
	app.init();
});