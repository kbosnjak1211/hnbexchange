app.types.Rates = Backbone.Collection.extend({
	
	url: app.prepareUrl('/historyRates'),
	model: app.types.Rate,
	
	initialize: function() {	
		this.fetch();
	},	
});
